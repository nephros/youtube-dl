import QtQuick 2.0
import Sailfish.Silica 1.0
import Nemo.Configuration 1.0
import Nemo.DBus 2.0
import org.nemomobile.lipstick 0.1

Page {
    id: page
    allowedOrientations: Orientation.All

    DBusInterface {
        id: settings
        bus: DBus.SessionBus
        service: "com.jolla.settings"
        path: "/com/jolla/settings/ui"
        iface: "com.jolla.settings.ui"
        //signalsEnabled: true
        function open() {
            typedCall("showPage", {
                          "type": "s",
                          "value": "system_settings/mime/youtube-dl"
                      }, function (result) {
                          console.debug('DBus call result: ' + result)
                      }, function (error, message) {
                          console.warn('DBus call failed: ', error,
                                      'message: ', message)
                      })
        }
    } //DBusInterface

    SilicaFlickable {
        id: flick
        anchors.fill: parent
        contentHeight: column.height

        PullDownMenu {
            MenuItem {
                //: about button
                //% "About"
                text: qsTrId("youtube-dl-clip-aboutlink")
                onClicked: {
                    pageStack.push(Qt.resolvedUrl("AboutPage.qml"))
                }
            }
            MenuItem {
                //: settings button
                //% "Settings"
                text: qsTrId("youtube-dl-clip-settingslink")
                onClicked: {
                    settings.open()
                }
            }
        }

        Column {
            id: column
            width: parent.width

            PageHeader {
                //: clipboard page title/header
                //% "Video URL download helper"
                title: qsTrId("youtube-dl-clip-header")
                id: ph
                anchors.horizontalCenter: parent.horizontalCenter
                Image {
                    anchors: {
                      left: parent.left
                      right: parent.right
                      margins: Theme.horizontalPageMargin
                      verticalCenter: ph.verticalCenter
                    }
                    source: conf.isActive ? "image://theme/icon-m-call-recording-on-light" : "image://theme/icon-m-call-recording-off?Theme.secondaryColor"
                    SequentialAnimation on opacity {
                        running: ( conf.isActive && page.status == PageStatus.Active )
                        loops: Animation.Infinite
                        alwaysRunToEnd: true
                        NumberAnimation { from: 0.25; to: 1.0; duration: 1500; easing.type: Easing.InOutQuad }
                        NumberAnimation { from: 1.0; to: 0.25; duration: 1500; easing.type: Easing.InOutQuad }
                        PauseAnimation { duration: 750 }
                    }
                }
            }
            Label {
                id: explanation
                padding: Theme.paddingLarge
                width: parent.width - Theme.paddingLarge
                anchors.horizontalCenter: parent.horizontalCenter
                //: short explanation below buttons
                //% "If what you have in your clipboard is a video URL, paste it here. You can either download right away, or modify the clipboard contents first, and then download."
                text: qsTrId("youtube-dl-clip-explain")
                color: Theme.highlightColor
                //horizontalAlignment: Text.AlignJustify
                wrapMode: Text.WordWrap
                truncationMode: TruncationMode.Elide
                font.pixelSize: Theme.fontSizeSmall
            }
            TextField {
                width: parent.width - Theme.paddingLarge
                id: clipField
                enabled: true
                softwareInputPanelEnabled: false
                focus: true
                inputMethodHints: Qt.ImhNoPredictiveText | Qt.ImhNoAutoUppercase
                horizontalAlignment: TextInput.AlignHCenter
                //placeholderText: Clipboard.text.length > 0 ? Clipboard.text : ""
                //: paste area
                //% "A video URL"
                label: qsTrId("youtube-dl-clip-paste-label")
                // TODO: do a little validation on names, we want this to be a simple word, no path, no variables etc.
                //validator: UrlValidator { }
                onClicked: {
                    softwareInputPanelEnabled: true
                }
                EnterKey.enabled: text.length > 0
                EnterKey.iconSource: "image://theme/icon-m-enter"
                EnterKey.onClicked: {
                    focus = false
                }
            }
            ButtonLayout {
                id: buttonRow
                Button {
                    id: pngButton
                    text: qsTrId("youtube-dl-clip-paste-button") + "+" + qsTrId(
                              "youtube-dl-clip-go-button")
                    enabled: clipField.text.length === 0
                    onClicked: {
                        //console.debug("paste+go clicked")
                        clipField.forceActiveFocus()
                        clipField.selectAll()
                        clipField.paste()
                        clipField.text = clipField.text.replace("https",
                                                                "ytdls")
                        clipField.text = clipField.text.replace("http", "ytdl")
                        console.info("opening URL: " + clipField.text)
                        conf.sync()
                        Qt.openUrlExternally(clipField.text)
                        clipField.text = ""
                        app.deactivate()
                    }
                }
                Button {
                    id: pogButton
                    text: {
                        if (clipField.text.length > 0) {
                            //: go button
                            //% "Go"
                            qsTrId("youtube-dl-clip-go-button")
                        } else {
                            //: paste button
                            //% "Paste"
                            qsTrId("youtube-dl-clip-paste-button")
                        }
                    }
                    onClicked: {
                        if (clipField.text.length === 0) {
                            //console.debug("paste clicked")
                            clipField.forceActiveFocus()
                            clipField.selectAll()
                            clipField.paste()
                            //pngButton.enabled = false
                            clipField.softwareInputPanelEnabled = true
                        } else {
                            //console.debug("go clicked")
                            clipField.text = clipField.text.replace("https",
                                                                    "ytdls")
                            clipField.text = clipField.text.replace("http",
                                                                    "ytdl")
                            console.info("opening URL: " + clipField.text)
                            conf.sync()
                            Qt.openUrlExternally(clipField.text)
                            clipField.text = ""
                            app.deactivate()
                        }
                    }
                }
            } //ButtonLayout
            SilicaItem {
                id: spacer
                height: Theme.itemSizeMedium
                width: parent.width - Theme.paddingLarge

                Separator {
                    color: Theme.primaryColor
                    horizontalAlignment: Qt.AlignHCenter
                }
            }
            TextSwitch {
                //width: parent.width - Theme.paddingLarge
                id: audioOption
                //: settings enable audio extraction
                //% "Only keep audio"
                text: qsTrId("youtube-dl-audio-only-enable")
                //: settings enable audio description
                //% "Download and extract only the audio track. It will be stored under"
                description: qsTrId("youtube-dl-audio-only-enable-desc")
                             + StandardPaths.music + "/" + conf.storagePath + "."

                //checked: pathField.text.length > 0
                checked: conf.audioOnly
                onClicked: {
                    conf.audioOnly = !conf.audioOnly
                }
            }
            Slider {
                visible: audioOption.checked
                width: parent.width - Theme.paddingLarge
                //:  audio quality slider
                //% "Audio Quality"
                label: qsTrId("youtube-dl-audio-quality")
                minimumValue: 0; maximumValue: 9; stepSize: 1
                handleVisible: down
                value: 5
                valueText: {
                  if ( value == 9 ) return "♫♫♫"
                  if ( value < 9 && value >= 6 ) return "♫♫"
                  if ( value < 6 && value >= 4 ) return "♪♫"
                  if ( value < 4 && value >= 1 ) return "♪♪"
                  if ( value == 0 ) return "♪"
                  //return value + "/9"
                }
                onReleased: {
                    conf.audioQuality = 9 - value
                }
            }
        } //Column
        //VerticalScrollDecorator { flickable: column }
    } //Flickable
} // end of Page

/*
kepp this here because of qtcreator autoformat
*/

//vim: expandtab ts=4 filetype=javascript
