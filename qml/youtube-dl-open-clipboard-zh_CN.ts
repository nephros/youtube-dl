<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name></name>
    <message id="youtube-dl-clip-cover">
        <source>Video\nDownloader</source>
        <extracomment>name displayed on cover</extracomment>
        <translation>视频\n下载器</translation>
    </message>
    <message id="youtube-dl-clip-aboutlink">
        <source>About</source>
        <extracomment>about button</extracomment>
        <translation>关于</translation>
    </message>
    <message id="youtube-dl-clip-about">
        <source>This is a convoluted collection of glue to trigger the excellent youtube-dl program. See below for links.</source>
        <extracomment>about text</extracomment>
        <translation>这是一个复杂的 glue 数据集用以打开优质的 youtube-dl 程序。请查看下方链接。</translation>
    </message>
    <message id="youtube-dl-clip-about-thanks">
        <source>Credits/Thanks/Links</source>
        <translation>信誉/致谢</translation>
    </message>
    <message id="youtube-dl-about-translations">
        <source>Translations</source>
        <translation>翻译者</translation>
    </message>
    <message id="youtube-dl-clip-settingslink">
        <source>Settings</source>
        <extracomment>settings button</extracomment>
        <translation>设置</translation>
    </message>
    <message id="youtube-dl-clip-header">
        <source>Video URL download helper</source>
        <extracomment>clipboard page title/header</extracomment>
        <translation>视频链接下载助手</translation>
    </message>
    <message id="youtube-dl-clip-explain">
        <source>If what you have in your clipboard is a video URL, paste it here. You can either download right away, or modify the clipboard contents first, and then download.</source>
        <extracomment>short explanation below buttons</extracomment>
        <translation>如果剪贴板里的是视频网址，请粘贴在此。你可以马上下载，也可以先修改剪贴板内容，然后再下载。</translation>
    </message>
    <message id="youtube-dl-clip-paste-label">
        <source>A video URL</source>
        <extracomment>paste area</extracomment>
        <translation>视频链接</translation>
    </message>
    <message id="youtube-dl-clip-paste-button">
        <source>Paste</source>
        <extracomment>paste button</extracomment>
        <translation>粘贴</translation>
    </message>
    <message id="youtube-dl-clip-go-button">
        <source>Go</source>
        <extracomment>go button</extracomment>
        <translation>前往</translation>
    </message>
    <message id="youtube-dl-audio-only-enable">
        <source>Only keep audio</source>
        <extracomment>settings enable audio extraction</extracomment>
        <translation>仅保存音频</translation>
    </message>
    <message id="youtube-dl-audio-only-enable-desc">
        <source>Download and extract only the audio track. It will be stored under</source>
        <oldsource>Download and extract only the audio track. It will be stored under ~/Music</oldsource>
        <extracomment>settings enable audio description</extracomment>
        <translation>下载并抽取音频。文件会保存于</translation>
    </message>
    <message id="youtube-dl-audio-quality">
        <source>Audio Quality</source>
        <extracomment>audio quality slider</extracomment>
        <translation>音频质量</translation>
    </message>
</context>
</TS>
