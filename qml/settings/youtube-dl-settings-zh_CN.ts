<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name></name>
    <message id="settings-youtube-dl-header">
        <source>Video URL download helper</source>
        <extracomment>settings page title/header</extracomment>
        <translation>视频链接下载助手</translation>
    </message>
    <message id="settings-youtube-dl-switch">
        <source>Enable downloading video URLs</source>
        <extracomment>settings page switch name</extracomment>
        <translation>开启下载视频链接</translation>
    </message>
    <message id="settings-youtube-dl-description">
        <source>Description of the feature here</source>
        <extracomment>settings menu switch description</extracomment>
        <translation>启用此功能将使浏览器和其它应用程序允许你为 URL 链接选择视频下载选项。

视频将被储存在 ~/Videos/youtube-dl 目录中，并显示于媒体应用程序内。

同时，我们会在之后尝试启动下载的文件来播放视频。如果远程方做了一些狡猾和邪恶的事情，可能会存在安全风险。
请负责任地下载。

注意，只有 youtube-dl 支持的 URL 才可以使用此项功能。</translation>
    </message>
    <message id="settings-system-mime-category">
        <source>MIME Handlers</source>
        <extracomment>the name of the settings category</extracomment>
        <translation>MIME 处理器</translation>
    </message>
    <message id="settings-youtube-dl-title">
        <source>Video Downloader</source>
        <extracomment>the name of the settings menu entry</extracomment>
        <translation>视频下载器</translation>
    </message>
    <message id="settings-youtube-dl-path-enable">
        <source>Specify storage location</source>
        <oldsource>Specify download location</oldsource>
        <extracomment>settings enable path input</extracomment>
        <translation>指定下载位置</translation>
    </message>
    <message id="settings-youtube-dl-path-enable-desc">
        <source>This should be a simple name without special characters to avoid breakage. It will be created under </source>
        <extracomment>settings enable path description</extracomment>
        <translation>此处应该是一个不含特殊字符的简单名称，以避免出现问题。它将被创建在</translation>
    </message>
    <message id="settings-youtube-dl-path-desc">
        <source>Path to store videos under</source>
        <extracomment>settings menu switch description</extracomment>
        <translation type="vanished">保存文件的路径</translation>
    </message>
    <message id="settings-youtube-dl-path-label">
        <source>A directory under: </source>
        <extracomment>settings menu switch label</extracomment>
        <translation>目录位于:</translation>
    </message>
    <message id="settings-youtube-dl-open">
        <source>Open file after download</source>
        <extracomment>settings page open file</extracomment>
        <translation>下载后打开文件</translation>
    </message>
    <message id="settings-youtube-dl-open-description">
        <source>This is a security risk</source>
        <extracomment>settings page open file description</extracomment>
        <translation>可能会有安全风险</translation>
    </message>
    <message id="video-quality">
        <source>Video Quality</source>
        <translation>视频质量</translation>
    </message>
    <message id="vq-automatic">
        <source>automatic \(best\)</source>
        <oldsource>automatic (best)</oldsource>
        <translation>自动(最佳)</translation>
    </message>
    <message id="audio-quality-vbr">
        <source>Audio Quality (VBR)</source>
        <translation type="vanished">音频质量(VBR)</translation>
    </message>
    <message id="prefer-free-formats">
        <source>Prefer Free formats</source>
        <translation>偏好自由格式</translation>
    </message>
    <message id="options">
        <source>Options</source>
        <translation>操作</translation>
    </message>
    <message id="vq-1080">
        <source>max 1080p</source>
        <translation>最大1080p</translation>
    </message>
    <message id="vq-720">
        <source>max 720p</source>
        <translation>最大720p</translation>
    </message>
    <message id="vq-480">
        <source>max 480p</source>
        <translation>最大480p</translation>
    </message>
    <message id="vq-worst">
        <source>worst</source>
        <translation>最糟</translation>
    </message>
    <message id="vq-all">
        <source>all available (use with caution!)</source>
        <translation>全部可用(请谨慎使用!)</translation>
    </message>
    <message id="settings-youtube-dl-prefer-use-netrc">
        <source>Respect ~/.netrc if it exists</source>
        <translation>遵守 ~/.netrc ，如果存在</translation>
    </message>
    <message id="settings-youtube-dl-prefer-use-netrc-description">
        <source>You can create a file called ~/.netrc and store user and password information there. This can be used to enable services which require a login to access. See the About page for more information.</source>
        <extracomment>netrc description</extracomment>
        <translation>你可以创建名为 ~/.netrc 的文件并在此储存用户及密码信息。此项功能可以用于需要登录才能访问的服务。查看关于页面以获取更多信息。</translation>
    </message>
    <message id="settings-youtube-dl-reset-remorse">
        <source>Clearing config settings</source>
        <oldsource>Clearing all config values</oldsource>
        <extracomment>reset remorse popup text</extracomment>
        <translation>清空所有配置值</translation>
    </message>
    <message id="settings-youtube-dl-reset">
        <source>Reset all to Default</source>
        <extracomment>reset button</extracomment>
        <translation>重置所有</translation>
    </message>
</context>
</TS>
