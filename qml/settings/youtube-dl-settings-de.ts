<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name></name>
    <message id="settings-system-mime-category">
        <source>MIME Handlers</source>
        <extracomment>the name of the settings category</extracomment>
        <translation>MIME Funktionen</translation>
    </message>
    <message id="settings-youtube-dl-title">
        <source>Video Downloader</source>
        <extracomment>the name of the settings menu entry</extracomment>
        <translation>Video Downloader</translation>
    </message>
    <message id="settings-youtube-dl-header">
        <source>Video URL download helper</source>
        <extracomment>settings page title/header</extracomment>
        <translation>Video-URL Herunterladehelferlein</translation>
    </message>
    <message id="settings-youtube-dl-switch">
        <source>Enable downloading video URLs</source>
        <extracomment>settings page switch name</extracomment>
        <translation>Herunterladen aktivieren</translation>
    </message>
    <message id="settings-youtube-dl-description">
        <source>Description of the feature here</source>
        <extracomment>settings menu switch description</extracomment>
        <translation>Wenn dies aktiv ist, bieten der Browser und andere Apps an, Videolinks herunterzuladen.

Es wird nur für URLs klappen, die von youtube-dl unterstützt werden.</translation>
    </message>
    <message id="settings-youtube-dl-open">
        <source>Open file after download</source>
        <extracomment>settings page open file</extracomment>
        <translation>Nach dem Herunterladen abspielen</translation>
    </message>
    <message id="settings-youtube-dl-open-description">
        <source>This is a security risk</source>
        <oldsource>This will try to open the downloaded file after downloading</oldsource>
        <extracomment>settings page open file description</extracomment>
        <translation>Das ist ein Sicherheitsrisiko, falls die Gegenseite Böses im Sinn hat.
Lade verantworungsvoll herunter!</translation>
    </message>
    <message id="settings-youtube-dl-path-desc">
        <source>Path to store videos under</source>
        <extracomment>settings menu switch description</extracomment>
        <translation type="vanished">Video-Ablageverzeichnis</translation>
    </message>
    <message id="settings-youtube-dl-path-label">
        <source>A directory under: </source>
        <extracomment>settings menu switch label</extracomment>
        <translation>Ein Verzeichnis unter: </translation>
    </message>
    <message id="settings-youtube-dl-path-enable">
        <source>Specify storage location</source>
        <oldsource>Specify download location</oldsource>
        <extracomment>settings enable path input</extracomment>
        <translation>Ablageverzeichis</translation>
    </message>
    <message id="settings-youtube-dl-path-enable-desc">
        <source>This should be a simple name without special characters to avoid breakage. It will be created under </source>
        <oldsource>This should be a simple name without special characters to avoid breakage</oldsource>
        <extracomment>settings enable path description</extracomment>
        <translation>Gib einen einfachen Verzeichnisnamen ohne spezielle Zeichen an. Es wird gespeichert unter</translation>
    </message>
    <message id="video-quality">
        <source>Video Quality</source>
        <translation>Bevorzugte Videoqualität</translation>
    </message>
    <message id="vq-automatic">
        <source>automatic \(best\)</source>
        <oldsource>automatic (best)</oldsource>
        <translation>Automatisch (beste)</translation>
    </message>
    <message id="audio-quality-vbr">
        <source>Audio Quality (VBR)</source>
        <oldsource>Audio Qualit (VBR)</oldsource>
        <translation type="vanished">Audioqualität (VBR)</translation>
    </message>
    <message id="prefer-free-formats">
        <source>Prefer Free formats</source>
        <translation>Freie/Offene Formate bevorzugen</translation>
    </message>
    <message id="options">
        <source>Options</source>
        <translation>Optionen</translation>
    </message>
    <message id="vq-1080">
        <source>max 1080p</source>
        <translation>Bis zu 1080p</translation>
    </message>
    <message id="vq-720">
        <source>max 720p</source>
        <translation>Bis zu 720p</translation>
    </message>
    <message id="vq-480">
        <source>max 480p</source>
        <translation>Bis zu 480p</translation>
    </message>
    <message id="vq-worst">
        <source>worst</source>
        <translation>Schlechteste (wieso nicht)</translation>
    </message>
    <message id="vq-all">
        <source>all available (use with caution!)</source>
        <translation>Alle verfügbaren (Vorsicht!)</translation>
    </message>
    <message id="settings-youtube-dl-prefer-use-netrc">
        <source>Respect ~/.netrc if it exists</source>
        <translation>Verwende ~/.netrc falls vorhanden</translation>
    </message>
    <message id="settings-youtube-dl-prefer-use-netrc-description">
        <source>You can create a file called ~/.netrc and store user and password information there. This can be used to enable services which require a login to access. See the About page for more information.</source>
        <oldsource>You can create a file called ~/.netrc and store user and password information there. This can be used to enable services which require a login to access. See the following URL for more information: </oldsource>
        <extracomment>netrc description</extracomment>
        <translation>Die Datei ~/.netrc kann verwendet werden, im Nutzer- und Passwortinformation zu speichern. Dies wird dann für Seiten verwendet, die ein Einloggen erfordern. Für mehr siehe die Seite &quot;About&quot; </translation>
    </message>
    <message id="settings-youtube-dl-reset-remorse">
        <source>Clearing config settings</source>
        <oldsource>Clearing all config values</oldsource>
        <extracomment>reset remorse popup text</extracomment>
        <translation>Setze Einstellungen zurück</translation>
    </message>
    <message id="settings-youtube-dl-reset">
        <source>Reset all to Default</source>
        <extracomment>reset button</extracomment>
        <translation>Einstellungen zurücksetzen</translation>
    </message>
</context>
</TS>
