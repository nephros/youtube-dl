<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name></name>
    <message id="settings-system-mime-category">
        <source>MIME Handlers</source>
        <oldsource>MIME Handlers</oldsource>
        <extracomment>the name of the settings category</extracomment>
        <translation>MIME Handlers</translation>
    </message>
    <message id="settings-youtube-dl-title">
        <source>Video Downloader</source>
        <oldsource>Video Downloader</oldsource>
        <extracomment>the name of the settings menu entry</extracomment>
        <translation>Video Downloader</translation>
    </message>
    <message id="settings-youtube-dl-header">
        <source>Video URL download helper</source>
        <oldsource>Video URL download helper</oldsource>
        <extracomment>settings page title/header</extracomment>
        <translation>Video URL download helper</translation>
    </message>
    <message id="settings-youtube-dl-switch">
        <source>Enable downloading video URLs</source>
        <oldsource>Enable downloading video URLs</oldsource>
        <extracomment>settings page switch name</extracomment>
        <translation>Enable downloading video URLs</translation>
    </message>
    <message id="settings-youtube-dl-description">
        <source>Description of the feature here</source>
        <extracomment>settings menu switch description</extracomment>
        <translation>Enabling this will cause the Browser and other apps to allow you to select a Video Download option for URL links.
Note that only URLs supported by youtube-dl will work.</translation>
    </message>
    <message id="settings-youtube-dl-path-enable">
        <source>Specify storage location</source>
        <oldsource>Specify download location</oldsource>
        <extracomment>settings enable path input</extracomment>
        <translation>Specify storage location</translation>
    </message>
    <message id="settings-youtube-dl-path-enable-desc">
        <source>This should be a simple name without special characters to avoid breakage. It will be created under </source>
        <oldsource>This should be a simple name without special characters to avoid breakage</oldsource>
        <extracomment>settings enable path description</extracomment>
        <translation>This should be a simple name without special characters to avoid breakage. It will be created under </translation>
    </message>
    <message id="settings-youtube-dl-path-desc">
        <source>Path to store videos under</source>
        <extracomment>settings menu switch description</extracomment>
        <translation type="vanished">Path to store videos under</translation>
    </message>
    <message id="settings-youtube-dl-path-label">
        <source>A directory under: </source>
        <extracomment>settings menu switch label</extracomment>
        <translation>A directory under: </translation>
    </message>
    <message id="settings-youtube-dl-open">
        <source>Open file after download</source>
        <extracomment>settings page open file</extracomment>
        <translation>Try to open the file after downloading</translation>
    </message>
    <message id="settings-youtube-dl-open-description">
        <source>This is a security risk</source>
        <oldsource>This will try to open the downloaded file after downloading</oldsource>
        <extracomment>settings page open file description</extracomment>
        <translation>This is a security risk if the remote party did something crafty and evil.
Download responsibly.</translation>
    </message>
    <message id="video-quality">
        <source>Video Quality</source>
        <translation>Preferred Video Quality</translation>
    </message>
    <message id="vq-automatic">
        <source>automatic \(best\)</source>
        <oldsource>automatic (best)</oldsource>
        <translation>automatic (best)</translation>
    </message>
    <message id="audio-quality-vbr">
        <source>Audio Quality (VBR)</source>
        <translation type="vanished">Audio Quality (VBR)</translation>
    </message>
    <message id="prefer-free-formats">
        <source>Prefer Free formats</source>
        <translation>Prefer Free formats</translation>
    </message>
    <message id="options">
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message id="vq-1080">
        <source>max 1080p</source>
        <translation>Up to 1080p</translation>
    </message>
    <message id="vq-720">
        <source>max 720p</source>
        <translation>Up to 720p</translation>
    </message>
    <message id="vq-480">
        <source>max 480p</source>
        <translation>Up to 480p</translation>
    </message>
    <message id="vq-worst">
        <source>worst</source>
        <translation>Worst available (because why not)</translation>
    </message>
    <message id="vq-all">
        <source>all available (use with caution!)</source>
        <translation>All available (use with caution!)</translation>
    </message>
    <message id="settings-youtube-dl-prefer-use-netrc">
        <source>Respect ~/.netrc if it exists</source>
        <translation>Respect ~/.netrc if it exists</translation>
    </message>
    <message id="settings-youtube-dl-prefer-use-netrc-description">
        <source>You can create a file called ~/.netrc and store user and password information there. This can be used to enable services which require a login to access. See the About page for more information.</source>
        <oldsource>You can create a file called ~/.netrc and store user and password information there. This can be used to enable services which require a login to access. See the following URL for more information: </oldsource>
        <extracomment>netrc description</extracomment>
        <translation>You can create a file called ~/.netrc and store user and password information there. This can be used to enable services which require a login to access. See the About page for more information.</translation>
    </message>
    <message id="settings-youtube-dl-reset-remorse">
        <source>Clearing config settings</source>
        <oldsource>Clearing all config values</oldsource>
        <extracomment>reset remorse popup text</extracomment>
        <translation>Clearing config settings</translation>
    </message>
    <message id="settings-youtube-dl-reset">
        <source>Reset all to Default</source>
        <extracomment>reset button</extracomment>
        <translation>Reset all to Default</translation>
    </message>
</context>
</TS>
