import QtQuick 2.0
import Sailfish.Silica 1.0
import Nemo.Configuration 1.0
import org.nemomobile.lipstick 0.1

ApplicationWindow {
    id: app

    ConfigurationGroup {
        id: conf
        path: "/org/nephros/youtube-dl/mime-helper"
        synchronous: true
        property bool audioOnly
        property bool isActive
        property string storagePath
    }
    initialPage: Qt.resolvedUrl("pages/MainPage.qml")
    cover: Qt.resolvedUrl("cover/CoverPage.qml")
    allowedOrientations: Orientation.All
} //end of ApplicationWindow

/*
//keep this here so the vim line below doesn't get moved up by qtcreator
*/

//vim: filetype=javascript expandtab ts=4

