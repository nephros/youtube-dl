<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name></name>
    <message id="youtube-dl-clip-paste-label">
        <source>A video URL</source>
        <extracomment>paste area</extracomment>
        <translation>Eine Video-URL</translation>
    </message>
    <message id="youtube-dl-clip-header">
        <source>Video URL download helper</source>
        <extracomment>clipboard page title/header</extracomment>
        <translation>Video-URL Herunterladehelferlein</translation>
    </message>
    <message id="youtube-dl-clip-cover">
        <source>Video\nDownloader</source>
        <extracomment>name displayed on cover</extracomment>
        <translation>Video Downloader</translation>
    </message>
    <message id="youtube-dl-clip-explain">
        <source>If what you have in your clipboard is a video URL, paste it here. You can either download right away, or modify the clipboard contents first, and then download.</source>
        <extracomment>short explanation below buttons</extracomment>
        <translation>Wenn deine Zwischenablage eine Video-URL enthält, füge sie hier ein. Du kannst das Herunterladen entweder sofort starten, oder den Inhalt erst editieren, und dann starten.</translation>
    </message>
    <message id="youtube-dl-clip-go-button">
        <source>Go</source>
        <extracomment>go button</extracomment>
        <translation>Los!</translation>
    </message>
    <message id="youtube-dl-clip-paste-button">
        <source>Paste</source>
        <extracomment>paste button</extracomment>
        <translation>Einfügen</translation>
    </message>
    <message id="youtube-dl-clip-about">
        <source>This is a convoluted collection of glue to trigger the excellent youtube-dl program. See below for links.</source>
        <oldsource>This is part of a collection of small utilities distributed on Openrepos. It mainly works because of a convoluted layer of glue to the excellent youtube-dl program. See below for links.</oldsource>
        <extracomment>about text</extracomment>
        <translation>Dies ist eine Sammlung von unterschiedlichsten Verbindungstechniken zum wunderbaren Programm Youtube-DL. Weblinks siehe unten.</translation>
    </message>
    <message id="youtube-dl-clip-aboutlink">
        <source>About</source>
        <extracomment>about button</extracomment>
        <translation>Über</translation>
    </message>
    <message id="youtube-dl-clip-settingslink">
        <source>Settings</source>
        <extracomment>settings button</extracomment>
        <translation>Einstellungen</translation>
    </message>
    <message id="youtube-dl-clip-about-thanks">
        <source>Credits/Thanks/Links</source>
        <translation>Credits/Danksagungen/Weblinks</translation>
    </message>
    <message id="youtube-dl-about-translations">
        <source>Translations</source>
        <translation>Übersetzungen</translation>
    </message>
    <message id="youtube-dl-audio-only-enable">
        <source>Only keep audio</source>
        <extracomment>settings enable audio extraction</extracomment>
        <translation>Nur Ton</translation>
    </message>
    <message id="youtube-dl-audio-only-enable-desc">
        <source>Download and extract only the audio track. It will be stored under</source>
        <oldsource>Download and extract only the audio track. It will be stored under ~/Music</oldsource>
        <extracomment>settings enable audio description</extracomment>
        <translation>Video herunterladen, Ton extrahieren und speichern. Die Datei entsteht unter </translation>
    </message>
    <message id="youtube-dl-audio-quality">
        <source>Audio Quality</source>
        <extracomment>audio quality slider</extracomment>
        <translation>Audioqualität</translation>
    </message>
</context>
</TS>
