#!/usr/bin/env bash
ytroot=$1
in=$2
out= $3
types=$(grep 'URL' "$ytroot"/youtube_dl/extractor/*py | grep -E -h -o 'http[s]*://[:alnum:]*[^/]+' | sed 's/\\//g;' | sed "s/'$//" | sed 's/(.*\?//g' | sed 's#http[s]*:/#x-url-handler#' | sed 's/$/;/' | sort -u | paste -sd ' ' | sed 's/ //g')
sed -e "s#\(^MimeType=.*\)#\1;${types}#" "$in"
